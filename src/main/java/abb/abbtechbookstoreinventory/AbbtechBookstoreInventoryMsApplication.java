package abb.abbtechbookstoreinventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbbtechBookstoreInventoryMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(AbbtechBookstoreInventoryMsApplication.class, args);
    }

}
