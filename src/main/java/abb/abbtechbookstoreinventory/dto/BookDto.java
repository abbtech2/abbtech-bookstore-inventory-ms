package abb.abbtechbookstoreinventory.dto;

import jakarta.persistence.Column;

import java.math.BigDecimal;

public record BookDto(String title,String author,BigDecimal price) {

}
