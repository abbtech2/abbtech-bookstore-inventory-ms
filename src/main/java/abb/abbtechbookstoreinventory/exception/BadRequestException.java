package abb.abbtechbookstoreinventory.exception;

import abb.abbtechbookstoreinventory.enums.BadRequestExceptionEnum;
import lombok.Data;

@Data
public class BadRequestException extends RuntimeException{

    private final BadRequestExceptionEnum badRequestExceptionEnum;
    public BadRequestException(BadRequestExceptionEnum badRequestExceptionEnum) {
        super(badRequestExceptionEnum.toString());
        this.badRequestExceptionEnum = badRequestExceptionEnum;
    }
    public BadRequestException(BadRequestExceptionEnum badRequestExceptionEnum, Throwable throwable) {
        super(badRequestExceptionEnum.toString(), throwable);
        this.badRequestExceptionEnum = badRequestExceptionEnum;
    }

}
