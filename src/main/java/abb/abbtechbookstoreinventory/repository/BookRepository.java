package abb.abbtechbookstoreinventory.repository;

import abb.abbtechbookstoreinventory.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BookRepository extends JpaRepository<Book, UUID> {
}
