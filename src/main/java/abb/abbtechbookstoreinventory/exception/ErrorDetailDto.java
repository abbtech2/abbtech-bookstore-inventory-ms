package abb.abbtechbookstoreinventory.exception;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ErrorDetailDto {

    String path;
    String errorMessage;
    String errorCode;
    Integer status;
    Date timeStamp;
}
