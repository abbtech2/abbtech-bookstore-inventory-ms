package abb.abbtechbookstoreinventory.service.impl;

import abb.abbtechbookstoreinventory.dto.BookDto;
import abb.abbtechbookstoreinventory.entity.Book;
import abb.abbtechbookstoreinventory.mapper.BookMapper;
import abb.abbtechbookstoreinventory.repository.BookRepository;
import abb.abbtechbookstoreinventory.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final BookMapper bookMapper;

    public BookDto addBook(BookDto bookDto) {
        Book book = bookMapper.bookDtoToBook(bookDto);
        bookRepository.save(book);
        return bookDto;
    }

    public List<BookDto> getAllBooks() {
        List<Book> allBooks = bookRepository.findAll();
        return allBooks.stream()
                .map(bookMapper::bookToBookDto)
                .collect(toList());
    }

    public Optional<BookDto> getBookById(UUID id) {
        return bookRepository.findById(id).map(bookMapper::bookToBookDto);
    }

    public BookDto updateBook(UUID id, BookDto updatedBook) {
        return bookRepository.findById(id)
                .map(book -> {
                    book.setTitle(updatedBook.title());
                    book.setAuthor(updatedBook.author());
                    book.setPrice(updatedBook.price());
                    Book savedBook = bookRepository.save(book);
                    return bookMapper.bookToBookDto(savedBook);
                })
                .orElseThrow(() -> new RuntimeException("Book not found"));
    }

    public void deleteBook(UUID id) {
        bookRepository.deleteById(id);
    }
}
