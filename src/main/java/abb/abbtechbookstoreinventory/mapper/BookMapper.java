package abb.abbtechbookstoreinventory.mapper;

import abb.abbtechbookstoreinventory.dto.BookDto;
import abb.abbtechbookstoreinventory.entity.Book;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BookMapper {

    BookDto bookToBookDto(Book book);
    Book bookDtoToBook(BookDto bookDto);

}
