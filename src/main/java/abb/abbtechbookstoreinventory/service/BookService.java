package abb.abbtechbookstoreinventory.service;

import abb.abbtechbookstoreinventory.dto.BookDto;
import abb.abbtechbookstoreinventory.entity.Book;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BookService {

    BookDto addBook(BookDto book);

    List<BookDto> getAllBooks();

    Optional<BookDto> getBookById(UUID id);

    BookDto updateBook(UUID id, BookDto updatedBook);

    void deleteBook(UUID id);
}
